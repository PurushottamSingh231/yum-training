import sqlite3

class dbConnection:

    @staticmethod
    def connection():
        try:
            db = sqlite3.connect('DataBase.sqlite')
            db.execute("CREATE TABLE IF NOT EXISTS UrlList(url TEXT NOT NULL,word_count Number ,url_id Text)")
            table = "CREATE TABLE IF NOT EXISTS WordDetails(unique_word TEXT PRIMARY KEY NOT NULL, "
            for i in range(206):
                table += 'url_id' + str(i) + ' NUMBER,'
            sql = table.strip(',') + ')'
            db.execute("{0}".format(sql))
            return db
        except sqlite3.Error:
            print("There is something wrong in connecting with database")
