import requests
from bs4 import BeautifulSoup


class linkDetail:

    def __init__(self):
        self.urlLink = set()
        # global set to store all the url present in the website whose link provided by the user

    def getLinksUtil(self, url):
        """
        :param url:
        :return: nothing
        This function is taking the url of the website as parameter and find all the url link
        present in that website. Here we have implemented simple BFS for traversing all the url
        and storing the visited url in urlLink set which has been globally initialize at class level

        Since we are storing the link in global variable urlLink this function will return nothing.
        This function has been called from main.py

        """
        visited = dict()
        queue = []
        queue.append(url)
        visited[url] = True

        while queue:
            s = queue.pop(0)
            self.urlLink.add(s)
            # print(s)

            page = requests.get(s)
            soup = BeautifulSoup(page.text, 'html.parser')
            links = soup.find_all('a')
            for link in links:
                localurl = link.get('href')
                if localurl is not None and localurl.startswith('https://www.sigmoid.com'):
                    temp = localurl.strip('/')
                    self.urlLink.add(temp)
                    if not visited.get(temp):
                        visited[temp] = False

            for i in self.urlLink:
                if not visited[i]:
                    queue.append(i)
                    visited[i] = True

    def getlistOfLink(self):
        """
        This function is utility function to get the list of all the url present in that particular
        website which user has given the link as input. It will not accept any formal parameter but will
        return the list of url which has been globally defined at class level.
        This function has been called from main.py
        :return: list of url as (urlLink)
        """
        return self.urlLink
