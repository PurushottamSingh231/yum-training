import re
import requests
from bs4 import BeautifulSoup



class wordCount:

    def __init__(self):
        pass

    def remove_tags(self,text):
        """
        This function takes a formal parameter text as string and remove all the tags with contents inside the tag from
        it and return a string consisting simple text only
        :param text:String
        :return: String
        """

        return re.sub(r'\d+', '', re.compile(r'<[^>]+>').sub('', text))

    def countDistinctWord(self,url):
        """
        This function takes a formal parameter url link parse that page using html parser and extract all the 'div' tags
        contents and then from each div tag contents removing the html tags which returns simple text from which we find
        all the word and stored into a set to maintain uniqueness of the word list and returning the word set to caller
        function
        :param url:
        :return:  set of words as (word) and number of unique word in the page as (len(word))
        """
        page = requests.get(url)
        soup = BeautifulSoup(page.text, 'html.parser')
        para = soup.find('div')
        word = set()
        if para is not None:
            try:
                for p in para:
                    s = self.remove_tags(str(p))
                    l = re.findall(r'\w+', s)
                    word.update(l)
            except KeyError:
                print("No Paragraph")
        return word, len(word)

    def getWordSec(self, url_link):
        """
        This funtion is a driver code for invoking the countDistinctWord() This function loops over all the link present
        in the websites and call the countDistinctWord function for each link and storing the return value of that
        function into word_list which is a python dictionary to maintain the key value pair for each link and
        corresponding details i.e. count of word and words present. it will return word_list to caller.

        :param url_link:
        :return: dictionary i.e key-value pair for each link as (word_list)
        """
        word_list = dict()
        # count_list = dict()
        for l in url_link:
            word, num = self.countDistinctWord(l)
            word_list[l] = [word, num]
            # count_list[l] = num
        return word_list

